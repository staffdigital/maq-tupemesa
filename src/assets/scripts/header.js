$(function(){
		//Mega Menu
		$('.header-search').clone().appendTo( ".menu-mobile-search" );
		$('.header-socials').clone().appendTo( ".menu-mobile-redes" );
		$('.header-search-wrapper').clone().appendTo( ".menu-mobile-fixed" );
		$('.header-link-hover').hover(
			function(){
				$('.header-menu-overlay').addClass('active');
				$('.header-fixed').addClass('hover');
			},
			function(){
				$('.header-menu-overlay').removeClass('active');
				$('.header-fixed').removeClass('hover');
			}
		);

		$('.btn-submenu-empresas').click(function(e){				
			e.preventDefault();		//evitar el eventos del enlace normal
			var strAncla=$(this).attr('href'); //id del ancla
				$('body,html').stop(true,true).animate({				
					scrollTop: $(strAncla).offset().top
				},1000);
			$('.header-menu-overlay').removeClass('active');
			$('.menu-sidebar').removeClass('active');
			
		});

		//- +++ validation of form +++
		$("form").validationEngine('attach', {
			promptPosition : "topLeft",
			autoHidePrompt: true,
			autoHideDelay: 3000,
			binded: false,
			scroll: false
		});

		$('.btn-form').on('click', function(event) {
			event.preventDefault();
			var valid = $("#form").validationEngine('validate');
			if (!valid) {
				//- return false;
			}
			else{
				//return true;
				$("#form").submit();
			}
		});
		//- <<< end >>>

		// formulario--inputs
		$('.soloNumber').each((k, el)=>{
			$(el).on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
		})
		$('.input-one').focusin((e) => {
			$(e.target).parent('.input-one-line').addClass('active')
		})
		.focusout((e) => {
			if(!e.target.value.length) {
				$(e.target).parent('.input-one-line').removeClass('active')
			}
		})
		$('.input-one').keyup((e) => {
			if(e.target.value.length) {
				$(e.target).parent('.input-one-line').addClass('active')
			}else{
				$(e.target).parent('.input-one-line').removeClass('active')
			}
		})

		//Input File
		$('.general__input__file--js').each(function(k, el){
			$(el).find('input[type="file"]').change(function(e){
				var id_input = $(this).attr('id');
				$('label[for="'+id_input+'"] .general__input__file__text').html('').html( e.target.files[0].name).addClass('active');
			});
		});

		$('.btn-header-cotiza').click(function(event) {
			event.preventDefault();
			$('.ctn-header-cotiza').addClass('active');
			$('.header-menu-overlay').addClass('active');
			$('body').addClass('active');
		});

		$('.btn-close-cotiza').click(function() {
			$('.ctn-header-cotiza').removeClass('active');
			$('.header-menu-overlay').removeClass('active');
			$('body').removeClass('active');
		});

		$('.header-menu-overlay').click(function() {
			$('.ctn-header-cotiza').removeClass('active');
			$('.header-menu-overlay').removeClass('active');
			$('body').removeClass('active');
			$('.header-search-wrapper').removeClass('active');
			$('.header-fixed').removeClass('hover');
			$('.menu-sidebar').removeClass('active');
			$('.menu-sidebar-categorias').removeClass('active');
		});

		$('.header-search').click(function() {
			$(this).toggleClass('active');
			$('.header-search-wrapper').addClass('active');
			$('.header-menu-overlay').addClass('active');
			$('body').addClass('active');
			$('.header-fixed').addClass('hover');
			$('.menu-mobile').addClass('hover');
			$('.header-search-wrapper-prin input').focus();
			if($(this).hasClass('active')){
				$('.btn-buscador').fadeOut('fast', function(){
					$('.btn-close-buscador').fadeIn('fast');
				})
			}else{
				$('.btn-close-buscador').fadeOut('fast', function(){
					$('.btn-buscador').fadeIn('fast');
				})
				$('.header-search-wrapper').removeClass('active');
				$('.header-menu-overlay').removeClass('active');
				$('.header-fixed').removeClass('hover');
				$('.menu-mobile').removeClass('hover');
				$('body').removeClass('active');
			}

		});


		//clone div's desktop to menu resposnsive 
		//$('.header-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header-list');
	
		//events: menu burguer
	

		$('.menu-mobile-close-header').click(function(event) {
			$('.menu-sidebar').removeClass('active');
			$('.header-menu-overlay').removeClass('active');
			$('body').removeClass('active');
		});	

	
		$('.menu-mobile-open').click(function(event) {
			$('.menu-sidebar').addClass('active');
			$('.header-menu-overlay').addClass('active');
			$('body').addClass('active');
		});

		$('.menu-mobile-close-productos').click(function(event) {
			$('.menu-sidebar').addClass('active');
			$('.menu-sidebar-categorias').removeClass('active');
			$('.header-menu-overlay').addClass('active');
		});

		$('.btn-mobile-header').click(function(event) {
	        event.preventDefault();
	        $('.menu-sidebar').removeClass('active');
	        var bdataid = $(this).attr('data-id');
	        $('.menu-sidebar-categorias').removeClass('active');
	        $('.menu-sidebar-categorias[id="'+bdataid+'"]').addClass('active');
	    });
		//detectando tablet, celular o ipad
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// tasks to do if it is a Mobile Device
			function readDeviceOrientation() {
				if (Math.abs(window.orientation) === 90) {
					// Landscape
					$('.ctn-header-cotiza').removeClass('active');
					$('.header-menu-overlay').removeClass('active');
					$('body').removeClass('active');
					$('.header-search-wrapper').removeClass('active');
					$('.header-fixed').removeClass('hover');
					$('.menu-sidebar').removeClass('active');
					$('.menu-sidebar-categorias').removeClass('active');
						} else {
					// Portrait
					$('.ctn-header-cotiza').removeClass('active');
					$('.header-menu-overlay').removeClass('active');
					$('body').removeClass('active');
					$('.header-search-wrapper').removeClass('active');
					$('.header-fixed').removeClass('hover');
					$('.menu-sidebar').removeClass('active');
					$('.menu-sidebar-categorias').removeClass('active');
				}
			}
			window.onorientationchange = readDeviceOrientation;
		}else{
			$(window).resize(function() {
				var estadomenu = $('.menu-responsive').width();
				if(estadomenu != 0){
					$('.ctn-header-cotiza').removeClass('active');
					$('.header-menu-overlay').removeClass('active');
					$('body').removeClass('active');
					$('.header-search-wrapper').removeClass('active');
					$('.header-fixed').removeClass('hover');
					$('.menu-sidebar').removeClass('active');
					$('.menu-sidebar-categorias').removeClass('active');
				}
			});
		}
		
	
		// header scroll
		var altoScroll = 0
		$(window).scroll(function() {
			altoScroll = $(window).scrollTop();
			if (altoScroll > 0) {
				$('.header-fixed').addClass('scrolling');
			}else{
				$('.header-fixed').removeClass('scrolling');
			};
		});
	});
	